﻿define(function() {
    ko.bindingHandlers.svgTransform = {
        init: function(element, valueAccessor) {
        },
        update: function(element, valueAccessor) {
            var value = valueAccessor(), scale;
            if (ko.unwrap(value)) {
                scale = 'scale(' + 0.05 * value() + ')';
                element.setAttribute('transform', scale);

            } else {
                scale = 'scale(' + 0 + ')';
                element.setAttribute('transform', scale);
            }
        }
    };
});