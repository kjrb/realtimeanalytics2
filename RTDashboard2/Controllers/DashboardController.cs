﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RTDashboard2.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /RTDashboard/
        public ActionResult Index(string theme, string id)
        {
            ViewBag.theme = theme;
            ViewBag.id = string.IsNullOrWhiteSpace(id) ? string.Empty : id;
            return View();
        }

        public ActionResult AnalyticsView(string theme, string id)
        {
            ViewBag.theme = theme;
            ViewBag.id = string.IsNullOrWhiteSpace(id) ? string.Empty : id;
            return View();
        }
	}
}