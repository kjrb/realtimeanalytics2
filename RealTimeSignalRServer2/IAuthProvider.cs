﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealTimeSignalRServer2
{
    public interface IAuthProvider 
    {                 
        bool Authenticate( string username, string password);        
    } 
}