﻿namespace RealTimeSignalRServer2
{
    using System.Security;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    //[Authorize]
    public class DashboardHub : BaseHub
    {
        public override Task OnConnected()
        {
            //if (this.AuthenticateUser("mySecretDashboard"))
            //{
            //    throw new SecurityException("User not authorized");
            //}

            AnalyticsHub.BroadcastUsers(Clients.Caller);
            AnalyticsHub.BroadcastUserSummary(Clients.Caller);
            return base.OnConnected();
        }

        public void Send(dynamic message)
        {
            AnalyticsHub.SendCommand(message);
        }
    }
}