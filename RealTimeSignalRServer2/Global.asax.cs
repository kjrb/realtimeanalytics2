﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.SignalR;


namespace RealTimeSignalRServer2
{
    using System.Diagnostics;

    using Microsoft.AspNet.SignalR.Hubs;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //var hubConfiguration = new HubConfiguration();
            //hubConfiguration.EnableCrossDomain = true;
            //hubConfiguration.EnableDetailedErrors = true;
            //hubConfiguration.EnableJavaScriptProxies = false;
            //RouteTable.Routes.MapHubs(hubConfiguration);

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalHost.HubPipeline.AddModule(new LoggingPipelineModule());
        }

        private class LoggingPipelineModule : HubPipelineModule
        {
            protected override bool OnBeforeIncoming(IHubIncomingInvokerContext context)
            {
                Debug.WriteLine("=> Invoking " + context.MethodDescriptor.Name + " on hub " + context.MethodDescriptor.Hub.Name);
                return base.OnBeforeIncoming(context);
            }

            protected override bool OnBeforeOutgoing(IHubOutgoingInvokerContext context)
            {
                Debug.WriteLine("<= Invoking " + context.Invocation.Method + " on client hub " + context.Invocation.Hub);
                return base.OnBeforeOutgoing(context);
            }

            //protected override bool OnBeforeConnect(IHub hub)
            //{
                //return base.OnBeforeConnect(hub);
            //}

            //protected override bool OnBeforeAuthorizeConnect(HubDescriptor hubDescriptor, IRequest request)
            //{
            //    return base.OnBeforeAuthorizeConnect(hubDescriptor, request);
            //}

            protected override bool OnBeforeAuthorizeConnect(HubDescriptor hubDescriptor, IRequest request)
            {
                if (this.ValidateAnalyticsHubUser(hubDescriptor, request) || this.ValidateDashboardHubUser(hubDescriptor, request))
                {
                    return base.OnBeforeAuthorizeConnect(hubDescriptor, request);
                }
                return false;
            }

            private bool ValidateAnalyticsHubUser(HubDescriptor hubDescriptor, IRequest request)
            {
                return request.QueryString != null && request.QueryString["myauthtoken"] == "mySecret"
                       && hubDescriptor.Name == "AnalyticsHub";
            }

            private bool ValidateDashboardHubUser(HubDescriptor hubDescriptor, IRequest request)
            {
                return request.QueryString != null && request.QueryString["myauthtoken"] == "mySecretDashboard"
                       && hubDescriptor.Name == "DashboardHub";
            }
        }
    }
}
