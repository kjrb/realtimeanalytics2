﻿namespace RealTimeSignalRServer2.Controllers
{
    using System.Web.Mvc;

    using RealTimeSignalRServer2.Models;

    public class AccountController : Controller
    {
        //
        // GET: /Account/
        //public ActionResult Index()
        //{
        //    return View();
        //}

        private IAuthProvider authProvider;

        // this should be ninject
        public AccountController() : this(new FormsAuthProvider())
        {
           
        }

        public AccountController(IAuthProvider auth)
        {
            this.authProvider = auth;
        }

        public ViewResult Login()
        {
            return this.View();
        }

        [HttpPost]
        public ActionResult Login( LoginViewModel model, string returnUrl) 
        {                         
            if (ModelState.IsValid) 
            {                                 
                if (authProvider.Authenticate( model.UserName, model.Password)) 
                {                                        
                    return Redirect( returnUrl ?? Url.Action("Index", "Admin"));                                
                } 
                else 
                {                                        
                    ModelState.AddModelError("", "Incorrect username or password");                                         
                    return View();                                
                }                        
            } 
            else 
            {                                 
                return View();                        
            }                
        }

        [Authorize]
        public ViewResult OK()
        {
            return this.View();
        }
    }
}