﻿var app = require('http').createServer(handler), io = require('socket.io').listen(app), path = require('path'), fs = require('fs'), und = require('underscore'), hubs = require('hubs/hubs');

io.configure(function () {
    io.set('transports', ['websocket']);
});

function handler(req, res) {
    fs.readFile(path.resolve(__dirname, 'index.html'),
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }

            res.writeHead(200);
            res.end(data);
        }
    );
}

hubs.init(io, und, 'node_');

app.listen(process.env.PORT || 8887);
