﻿define(['jquery', 'knockout', 'signalR', 'socketio', 'messageBuilder'], function (jq, ko, signalR, socketio, messageBuilder) {
    console.log("jQuery version: ", $.fn.jquery);
    console.log("Knockout version: ", ko.version);
    console.log("SignalR version: ", $.signalR.version);
    console.log("SocketIO version: ", io.version);
    //console.log("SignalR version: ", signalR.version);
    console.log("SocketIO version: ", socketio.version);

    var universalClient = (function () {
        var that = this;
        this.useSignalR = ko.observable();
        this.useNodejs = ko.observable();
        this.useIISnode = ko.observable();
        that.isConnected = ko.observable();
        this.sendHandlers = [];
        
        var msg = ko.observable();
        
        this.logger = function () {
            this.log = function (message) {
                console.log(message);
            };
            return this;
        }();
        
        this.init = function (config) {
            that.useSignalR(config.useSignalR);
            that.useNodejs(config.useNodejs);
            that.useIISnode(config.useIISnode);
        };

        this.startConnections = function() {
            if (that.useSignalR()) {
                that.sendHandlers.push(signalRInit(that.useSignalR));
            }
            if (that.useNodejs()) {
                that.sendHandlers.push(nodejsInit(that.useNodejs));
            }
            if (that.useIISnode()) {
                that.sendHandlers.push(iisnodeInit(that.useIISnode));
            }
            that.isConnected(true);
        };
        
        function signalRInit(shouldSend) {
            try {
                var ok = false;
                var url = 'http://localhost/RealTimeSignalRServer2';
                var connection = $.hubConnection(url);
                connection.logging = true;
                connection.qs = "myauthtoken=mySecret";
                var proxy = connection.createHubProxy('analyticshub');
                proxy.on('message', function(data) {
                    processReceivedData(data);
                });
                connection.start({ transport: 'webSockets' })
                    .done(function() {
                        that.logger.log('Now connected to ' + url + ', connection ID=' + connection.id + ' UniqueId=');
                        ok = true;
                    })
                    .fail(function(err) {
                        that.logger.log('Could not connect to ' + url + " " + err);
                    });

                that.disconnetHandler = function() {
                    that.logger.log('Disconnected from ' + url);
                    ok = false;
                };

                function send(data) {
                    if (!ok || !shouldSend()) {
                        that.logger.log('Not sending to ' + url);
                        return;
                    }

                    proxy.invoke('send', data).done(function() {
                        that.logger.log('Invocation of send to ' + url + ' succeeded');
                    }).fail(function(error) {
                        that.logger.log('Invocation of send to ' + url + ' failed. Error: ' + error);
                    });
                }

                return {
                    send: send
                };
            } catch(ex) {
                return that.logger.log('Error: ' + ex.message);
            }
        }

        function nodejsInit(shouldSend) {
            var url = 'http://localhost:8887/analyticshub?myauthtoken=mySecret';
            return nodejsCommon(url, shouldSend);
        }

        function iisnodeInit(shouldSend) {
            var url = 'http://localhost:9998/analyticshub?myauthtoken=mySecret';
            return nodejsCommon(url, shouldSend);
        }

        function nodejsCommon(url, shouldSend) {
            try {
                var ok = false;
                var client = io.connect(url, { 'sync disconnect on unload': true });

                client.on('message', function(data) {
                    processReceivedData(data);
                });

                client.on('connect', function() {
                    that.logger.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
                    ok = true;
                });

                client.on('connect_failed', function(reason) {
                    that.logger.log('Could not connect to ' + url + ": " + reason);
                });

                that.disconnetHandler = function() {
                    that.logger.log('Disconnected from ' + url);
                    if (ok) {
                         client.disconnect();
                    }
                    ok = false;
                };

                function send(data) {
                    if (!ok || !shouldSend()) {
                        that.logger.log('Not sending to ' + url);
                        return;
                    }
                    client.emit('send', data);
                    that.logger.log('Invocation of send to ' + url + ' succeeded');
                }

                return {
                    send: send
                };
            } catch(ex) {
                return null;
            }
        }

        function processReceivedData(data) {
            if (data.type === 'greeting') {
                that.logger.log('unique id: ' + data.message);
            }

            var commandResponse, commandMessage;
            if (data.type === 'command') {
                switch(data.message) {
                    case 'browser':
                       commandResponse = navigator.userAgent;
                }
                if (commandResponse) {
                    commandMessage = { type: 'command', subtype: data.message, message: { tm: (new Date()).valueOf(), comamnd: data.message, response: commandResponse } };
                    sendMessage(commandMessage);
                    that.logger.log('command data send: ' + commandMessage);
                }
            }
        }

        function sendClickMessage() {
            var messageToSend = messageBuilder.buildClickMessage();
            msg(JSON.stringify(messageToSend));
            sendMessage(messageToSend);
        }

        function sendMessage(toSend) {
            $.each(that.sendHandlers, function (i, handler) {
                //var toSend = msg();
                if (handler && handler.send && $.isFunction(handler.send)) {
                    handler.send(toSend);
                }
            });
        }
       
        function sendServiceCallData() {
            var msgToSend = messageBuilder.buildServiceCallMessage();
            msg(JSON.stringify(msgToSend));
            sendMessage(msgToSend);
        }

        function sendPerformanceData() {
            var msgToSend = messageBuilder.buildPerformanceMessage();
            msg(JSON.stringify(msgToSend));
            sendMessage(msgToSend);
        }
        
        return {
            init: init,
            sendClickData: function() {
                sendClickMessage();
            },
            sendServiceCallData: function() {
                sendServiceCallData();
            },
            sendPerformanceData: function() {
                sendPerformanceData();
            },
            message: msg
        };
    })();

    return universalClient;
});